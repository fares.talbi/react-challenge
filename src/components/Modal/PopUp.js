import React from "react";
import classes from "./Popup.module.css";
import Card from '../UI/Card'

const PopUp = (props) => {
  return (
    <Card className={classes.modal}>
    <div>
      <header className={classes.header}>
        <h2 style={{ color: "#0A0F8D" }}>{props.subject.nom}</h2>
      </header>
      <div className={classes.content}>
        <h6>{props.subject.description}</h6>
      </div>

      <footer className={classes.actions}>
        <button className="btn btn-primary" onClick={props.fermer}>
          Fermer
        </button>
      </footer>
    </div>
    </Card>
  );
};

export default PopUp;
