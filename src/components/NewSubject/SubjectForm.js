import React, { useState } from "react";
import "./SubjectForm.css";

const SubjectForm = (props) => {
  const [enteredName, setEnteredName] = useState("");
  const [enteredDescription, setEnteredDescription] = useState("");

  const submitHandler = (e) => {
    e.preventDefault();

    const subjectData = {
      nom: enteredName,
      description: enteredDescription,
    };

    props.onSaveSubjectData(subjectData);

    setEnteredName("");
    setEnteredDescription("");
  };
  return (
      <div className="registration-form">
        <form onSubmit={submitHandler}>
          <div className="title">
            <h4>Ajouter une matiére</h4>
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control item"
              placeholder="Nom"
              value={enteredName}
              onChange={(e) => setEnteredName(e.target.value)}
            />
          </div>
          <div className="form-group">
            <textarea
              className="form-control item"
              placeholder="Description"
              value={enteredDescription}
              onChange={(e) => setEnteredDescription(e.target.value)}
            />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-block create-account">
              Ajouter
            </button>
          </div>
        </form>
        <div className="social-media"></div>
      </div>
  );
};

export default SubjectForm;
