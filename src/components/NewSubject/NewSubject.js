import React from 'react';
import SubjectForm from './SubjectForm';
import './NewSubject.css'

const NewSubject = (props) => {

    const saveSubjectDataHandler = (enteredSubjectData) => {
        const subjectData = {
            ...enteredSubjectData,
            id: Math.random().toString()
        }
            props.onAddSubject(subjectData);
    }

    return (
        <div className="col-lg-6">
        <div className="right-image">
           <SubjectForm onSaveSubjectData={saveSubjectDataHandler}/>
           </div>
           </div>
    );
};

export default NewSubject;