import React from "react";
import { Fragment } from "react";
import  './SubjectList.css'
const SubjectItem = (props) => {
  return (
    <Fragment>
      <div className="col-lg-4">
        <div className="box-item">
          <h4>
            <a onClick={props.popup}>{props.nom}</a>
          </h4>
          <p>{props.description}</p>
        </div>
      </div>
      
    </Fragment>
  );
};

export default SubjectItem;
