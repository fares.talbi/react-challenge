import React, { useState } from "react";
import { Fragment } from "react";
import PopUp from "../Modal/PopUp";
import SubjectForm from "../NewSubject/SubjectForm";
import SubjectItem from "./SubjectItem";
import "./SubjectList";

const SubjectList = (props) => {
  const [isValid, setIsValid] = useState(false);
  const [subject, setSubject] = useState({});

  const fermer = () => {
    setIsValid(false);
  };

  return (
    <Fragment>
            <div className="col-lg-6 align-self-center">
              <div className="section-heading">
                <h4>
                  Taki<em>Academy</em>
                </h4>
                <h6>Il ya 5 minutes.</h6>
              </div>
              <div className="row">
                {props.subjects.map((subject) => {
                  return (
                    <SubjectItem
                      key={subject.id}
                      nom={subject.nom}
                      description={subject.description}
                      popup={() => {
                        setIsValid(true);
                        setSubject(subject);
                      }}
                    />
                  );
                })}
              </div>
            </div>
            {isValid && <PopUp subject={subject} fermer={fermer} />}
    </Fragment>
  );
};

export default SubjectList;
