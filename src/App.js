import { useState } from "react";
import NewSubject from "./components/NewSubject/NewSubject";
import SubjectList from "./components/Subjects/SubjectList";

const subject_data = [
  {
    id: "1",
    nom: "Maths",
    description:
      "On sait depuis longtemps que travailler avec du descriptione lisible",
  },
  {
    id: "2",
    nom: "Physique",
    description:
      "On sait depuis longtemps que travailler avec du descriptione lisible",
  },
  {
    id: "3",
    nom: "SVT",
    description:
      "On sait depuis longtemps que travailler avec du descriptione lisible",
  },
  {
    id: "4",
    nom: "Arabe",
    description:
      "On sait depuis longtemps que travailler avec du descriptione lisible",
  },
  {
    id: "5",
    nom: "Anglais",
    description:
      "On sait depuis longtemps que travailler avec du descriptione lisible",
  },
];

function App() {
  const [subjects, setSubjects] = useState(subject_data);

  const addSubjectHandler = (subject) => {
    setSubjects((prevSubjects) => {
      return [...prevSubjects, subject];
    });
  };
  console.log(subjects);

  const deleteHandler = (id) => {
    setSubjects((prevSubjects) => {
      const updateSubjects = [...prevSubjects].filter(subject => subject.id !== id);
      return updateSubjects;
      console.log(updateSubjects);
    })
  }
  
  return (
    <div id="about" className="about-us section">
    <div className="container">
      <div className="row">
      <SubjectList subjects={subjects} onDelete={deleteHandler}/>
      <NewSubject onAddSubject={addSubjectHandler} />
    </div>
    </div>
    </div>
 
  );
}

export default App;
